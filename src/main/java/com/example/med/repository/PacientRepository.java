package com.example.med.repository;

import com.example.med.model.Plan;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.med.model.Pacient;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface PacientRepository extends JpaRepository<Pacient , Long> {

    public Pacient findByName(@Param("name") String name);

    public List<Pacient> findAllByCaregiverName(@Param("caregiverName") String caregiverName);

}
