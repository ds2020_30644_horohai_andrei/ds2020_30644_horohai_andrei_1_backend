package com.example.med.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.med.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
   Optional<User> findByName(@Param("name") String name);

   // public User findByName(@Param("name") String name);

    Boolean existsByName(@Param("name") String name);

}