package com.example.med.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.med.model.Medicament;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicamentRepository extends  JpaRepository<Medicament , Long> {

}
