package com.example.med.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.med.model.Plan;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface PlanRepository extends JpaRepository<Plan , Long> {

    public List<Plan> findAllByPacientId(@Param("pacientId") int pacientId);

}
