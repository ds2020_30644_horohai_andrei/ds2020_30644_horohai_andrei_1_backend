package com.example.med.controller;

import com.example.med.exception.ResourceNotFoundException;
import com.example.med.model.Caregiver;
import com.example.med.model.Pacient;
import com.example.med.repository.CaregiverRepository;

import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")

public class CaregiverController {

    @Autowired
    private CaregiverRepository caregiverRepository;

    //get all Caregivers

    @GetMapping("/caregivers")
    public List<Caregiver> getAllCaregivers(){
        return caregiverRepository.findAll();
    }


    //create caregiver rest api
    @PostMapping("/caregivers")
    public Caregiver createCaregiver(@RequestBody Caregiver caregiver){

        return caregiverRepository.save(caregiver);

    }

    //get caregiver by id rest api
    @GetMapping("/caregivers/{id}")
    public ResponseEntity<Caregiver> getCaregiverById(@PathVariable Long id){

        Caregiver caregiver = caregiverRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Caregiver doesn`t exist with id:"+id));

        return ResponseEntity.ok(caregiver);

    }

    //update caregiver rest api
    @PutMapping("/caregivers/{id}")
    public ResponseEntity<Caregiver> updateCaregiver(@PathVariable Long id, @RequestBody Caregiver caregiverDetails){

        Caregiver caregiver = caregiverRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Caregiver doesn`t exist with id:"+id));

        caregiver.setName(caregiverDetails.getName());
        caregiver.setBirth(caregiverDetails.getBirth());
        caregiver.setGender(caregiverDetails.getGender());
        caregiver.setAddress(caregiverDetails.getAddress());
        caregiver.setPatients(caregiverDetails.getPatients());

        Caregiver updatedCaregiver = caregiverRepository.save(caregiver);
        return ResponseEntity.ok(updatedCaregiver);

    }

    //delete caregiver rest api
    @DeleteMapping("/caregivers/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteCaregiver(@PathVariable Long id){

        Caregiver caregiver = caregiverRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Caregiver doesn`t exist with id:"+id));


        caregiverRepository.delete(caregiver);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return ResponseEntity.ok(response);

    }

    //get caregiver by name
    @GetMapping("/caregivers/CaregiverDetails")
    public ResponseEntity<Caregiver> getCaregiverByName(@RequestParam String caregiverName){

        Caregiver caregiverr = caregiverRepository.findByName(caregiverName);

        return ResponseEntity.ok(caregiverr);
    }




}
