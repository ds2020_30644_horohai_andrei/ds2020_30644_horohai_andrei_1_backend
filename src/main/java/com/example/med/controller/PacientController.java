package com.example.med.controller;

import com.example.med.exception.ResourceNotFoundException;
import com.example.med.model.Pacient;
import com.example.med.repository.PacientRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class PacientController {

    @Autowired
    private PacientRepository pacientRepository;

    //get all Pacients

    @GetMapping("/pacienti")
    public List<Pacient> getAllPacienti(){
        return pacientRepository.findAll();
    }

    //create pacient rest api
    @PostMapping("/pacienti")
    public Pacient createPacient(@RequestBody Pacient pacient) {
        return pacientRepository.save(pacient);
    }

    //get pacient by id rest api
    @GetMapping("/pacienti/{id}")
    public ResponseEntity<Pacient> getPacientById(@PathVariable Long id){

        Pacient pacient = pacientRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Pacient not exist with id:"+id));

        return ResponseEntity.ok(pacient);
    }


    //uppate pacient rest api
    @PutMapping("/pacienti/{id}")
    public ResponseEntity<Pacient> updatePacient(@PathVariable Long id , @RequestBody Pacient pacientDetails){

        Pacient pacient = pacientRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Pacient not exist with id:"+id));

        pacient.setName(pacientDetails.getName());
        pacient.setBirth(pacientDetails.getBirth());
        pacient.setGender(pacientDetails.getGender());
        pacient.setAddress(pacientDetails.getAddress());
        pacient.setRecord(pacientDetails.getRecord());
        pacient.setCaregiverName(pacientDetails.getCaregiverName());

        Pacient updatedPacient =  pacientRepository.save(pacient);
        return ResponseEntity.ok(updatedPacient);
    }

    //delete pacient rest api
    @DeleteMapping("/pacienti/{id}")
    public ResponseEntity<Map<String,Boolean>> deletePacient(@PathVariable Long id){

        Pacient pacient = pacientRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Pacient not exist with id:"+id));

        pacientRepository.delete(pacient);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted",Boolean.TRUE);
        return ResponseEntity.ok(response);

    }

    //get pacient by name
    @GetMapping("/pacienti/DetailsByName")
    public ResponseEntity<Pacient> getPacientByName(@RequestParam String pacientName){

        Pacient pacientr = pacientRepository.findByName(pacientName);

        return ResponseEntity.ok(pacientr);
    }


    @GetMapping("/pacienti/GetPacientsByCaregvier")
    public List<Pacient> findAllByCaregiverName(@RequestParam String caregiverName){

        return pacientRepository.findAllByCaregiverName(caregiverName);
    }





}
