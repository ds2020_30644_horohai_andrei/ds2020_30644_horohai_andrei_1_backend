package com.example.med.controller;

import com.example.med.exception.ResourceNotFoundException;
import com.example.med.model.Medicament;
import com.example.med.repository.MedicamentRepository;

import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")

public class MedicamentController {

    @Autowired
    private MedicamentRepository medicamentRepository;


    //get all medicamente

    @GetMapping("/medicamente")
    public List<Medicament> getAllMedicamente(){
        return medicamentRepository.findAll();
    }

    //create medicament resti api
    @PostMapping("/medicamente")
    public Medicament createMedicament(@RequestBody Medicament medicament){
        return medicamentRepository.save(medicament);
    }


    //get medicament by id rest api
    @GetMapping("/medicamente/{id}")
    public  ResponseEntity<Medicament> getMedicamentById(@PathVariable Long id){

        Medicament medicament = medicamentRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Medicament doesn't exist with id:"+id));

        return ResponseEntity.ok(medicament);

    }

    //update medicament rest api
    @PutMapping("/medicamente/{id}")
    public ResponseEntity<Medicament> updateMedicament(@PathVariable Long id, @RequestBody Medicament medicamentDetails){
        Medicament medicament = medicamentRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Medicament doesn`t exist with id:"+id));

        medicament.setName(medicamentDetails.getName());
        medicament.setSide(medicamentDetails.getSide());
        medicament.setDosage(medicamentDetails.getDosage());

        Medicament updatedMedicament = medicamentRepository.save(medicament);
        return ResponseEntity.ok(updatedMedicament);


    }

    //delete medicament rest api
    @DeleteMapping("/medicamente/{id}")
    public ResponseEntity<Map<String , Boolean>> deleteMedicament(@PathVariable Long id){

        Medicament medicament = medicamentRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Medicament doesn`t exist with id:"+id));

        medicamentRepository.delete(medicament);
        Map<String , Boolean> response  = new HashMap<>();
        response.put("deleted",Boolean.TRUE);
        return ResponseEntity.ok(response);
    }

}
