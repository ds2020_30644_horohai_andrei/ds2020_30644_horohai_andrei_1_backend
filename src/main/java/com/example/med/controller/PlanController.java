package com.example.med.controller;

import com.example.med.exception.ResourceNotFoundException;
import com.example.med.model.Pacient;
import com.example.med.model.Plan;
import com.example.med.repository.PacientRepository;
import com.example.med.repository.PlanRepository;

import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import java.util.UUID;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")


public class PlanController {

    @Autowired
    private PlanRepository planRepository;

    //create plan rest api
    @PostMapping("/plans")
    public Plan createPlan(@RequestBody Plan plan){
        return planRepository.save(plan);
    }

    @GetMapping("/plans/{pacientId}")
    public ResponseEntity<Plan> getPlansByPacientId(@PathVariable Long pacientId){

        Plan plan = planRepository.findById(pacientId).orElseThrow(()->new ResourceNotFoundException("Plan doesn't exist with pacientId:"+pacientId));
        return ResponseEntity.ok(plan);
    }

    @GetMapping("/plans/PlanById")
    public List<Plan> getPlanByPacientId(@RequestParam int pacientId){

        return planRepository.findAllByPacientId(pacientId);
    }



}
