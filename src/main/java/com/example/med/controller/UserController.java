package com.example.med.controller;


import com.example.med.exception.ResourceNotFoundException;
import com.example.med.model.User;
import com.example.med.model.MessageResponse;
import com.example.med.repository.UserRepository;
import org.apache.logging.log4j.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//import javax.validation.Valid;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")

public class UserController {


    @Autowired
    private UserRepository userRepository;

    //get user role (login)
    @GetMapping("/users")
    public ResponseEntity<User> getUserByName(@RequestParam String userName, @RequestParam String userPassword) {

        User userr = userRepository.findByName(userName).orElseThrow(() -> new ResourceNotFoundException("User doesn`t exist with name:" + userName));
        if(!userr.getPassword().equals(userPassword))
        {
            throw (new ResourceNotFoundException("username/password wrong"));
        }




        return ResponseEntity.ok(userr);


    }
}
